package com.example.cybosoltest.Model;

/**
 * Created by Nipun on 3/23/2017.
 */


public class Name {
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "ClassPojo [label = " + label + "]";
    }
}