package com.example.cybosoltest.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nipun on 3/23/2017.
 */

public class Entry
{

    private Summary summary;
    private Title title;
    @SerializedName("im:price")
    private Price price;
    @SerializedName("im:name")
    private Name name;
    @SerializedName("im:image")
    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }


    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Summary getSummary ()
   {
       return summary;
   }

    public void setSummary (Summary summary)
    {
        this.summary = summary;
    }
    public Title getTitle ()
    {
        return title;
    }

    public void setTitle (Title title)
    {
        this.title = title;
    }


}
