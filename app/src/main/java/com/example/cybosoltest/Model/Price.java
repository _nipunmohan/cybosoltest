package com.example.cybosoltest.Model;

/**
 * Created by Nipun on 3/23/2017.
 */

public class Price {
    private String label;
    private Attributes attributes;
    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public class Attributes{
        private String amount;
        private String currency;

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
    @Override
    public String toString()
    {
        return "ClassPojo [label = "+label+"]";
    }
}
