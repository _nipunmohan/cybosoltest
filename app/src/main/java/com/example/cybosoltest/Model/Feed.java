package com.example.cybosoltest.Model;

import java.util.List;

/**
 * Created by Nipun on 3/23/2017.
 */


public class Feed
{
    private List<Entry> entry;

    public List<Entry> getEntry ()
    {
        return entry;
    }

    public void setEntry (List<Entry> entry)
    {
        this.entry = entry;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [entry = "+entry+"]";
    }
}
