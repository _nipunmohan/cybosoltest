package com.example.cybosoltest.Model;

/**
 * Created by Nipun on 3/23/2017.
 */


public class FeedModel {

    private Feed feed;

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    @Override
    public String toString() {
        return "ClassPojo [feed = " + feed + "]";
    }
}