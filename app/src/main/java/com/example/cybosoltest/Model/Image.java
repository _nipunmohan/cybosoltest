package com.example.cybosoltest.Model;

/**
 * Created by Nipun on 3/23/2017.
 */

public class Image {
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
