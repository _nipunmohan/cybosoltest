package com.example.cybosoltest.Presenter;

import android.content.Context;

import com.example.cybosoltest.Model.FeedModel;
import com.example.cybosoltest.Service.DataService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nipun on 3/23/2017.
 */

public class DataPresenter {
    private final Context context;
    private final DataPresenterListener mListener;
    private final DataService dataService;

    public interface DataPresenterListener {
        void feedsReady(FeedModel entries);
    }

    public DataPresenter(DataPresenterListener listener, Context context){
        this.mListener = listener;
        this.context = context;
        this.dataService = new DataService();
    }

    public void getFeeds(){
        dataService
                .getAPI()
                .getResults()
                .enqueue(new Callback<FeedModel>() {
                    @Override
                    public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
                        FeedModel result = response.body();

                        if(result != null)
                            mListener.feedsReady(result);
                    }

                    @Override
                    public void onFailure(Call<FeedModel> call, Throwable t) {
                        try {
                            throw  new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
