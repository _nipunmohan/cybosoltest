package com.example.cybosoltest.Service;

import com.example.cybosoltest.Model.FeedModel;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by jean on 29/07/16.
 */

public class DataService {
    private static String BASE_URL =  "http://ax.itunes.apple.com/";

    public interface DataAPI {
        @GET("WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/sf=143441/limit=100/genre=6007/json")
        Call<FeedModel> getResults();
    }

    public DataAPI getAPI(){
        Retrofit retrofit = new Retrofit
                            .Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
        return retrofit.create(DataAPI.class);
    }
}
