package com.example.cybosoltest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cybosoltest.Data.DataStore;
import com.example.cybosoltest.Model.Entry;
import com.example.cybosoltest.Model.FeedModel;
import com.example.cybosoltest.Presenter.DataPresenter;

import io.realm.Realm;
import io.realm.RealmResults;
/**
 * Created by Nipun on 3/23/2017.
 */



public class MainActivity extends AppCompatActivity implements DataPresenter.DataPresenterListener {

    private DataPresenter dataPresenter;
    private UiAdapter uiAdapter;
    private TextView mtxtErrorView;
    private ListView mListView;
    private Realm mRealm;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        mRealm = Realm.getInstance(getApplicationContext());
        mListView = (ListView) findViewById(R.id.listViewPosts);
        mtxtErrorView = (TextView) findViewById(R.id.txtErrorView);
        mtxtErrorView.setVisibility(View.GONE);
        if (isNetworkAvailable()) {
            mListView.setVisibility(View.GONE);
            spinner.setVisibility(View.VISIBLE);
            dataPresenter = new DataPresenter(this, this);
            dataPresenter.getFeeds();
        } else {
            if (mRealm.isEmpty()) {
                mListView.setVisibility(View.GONE);
                mtxtErrorView.setVisibility(View.VISIBLE);
            } else {
                spinner.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
                RealmResults<DataStore> dataStore = mRealm.where(DataStore.class).findAll();
                uiAdapter = new UiAdapter(this, dataStore);
                mListView.setAdapter(uiAdapter);
                mListView.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Persisted Data", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void feedsReady(FeedModel entries) {

        Toast.makeText(getApplicationContext(), "Persisting Data", Toast.LENGTH_LONG).show();
        mRealm.beginTransaction();
        mRealm.where(DataStore.class).findAll().clear();
        mRealm.commitTransaction();
        for (Entry entry : entries.getFeed().getEntry()) {
            mRealm.beginTransaction();
            DataStore data = mRealm.createObject(DataStore.class);
            data.setName(entry.getName().getLabel());
            data.setPrice(entry.getPrice().getAttributes().getAmount());
            data.setCurrency(entry.getPrice().getAttributes().getCurrency());
            data.setSummary(entry.getSummary().getLabel());
            data.setImageUrl(entry.getImages().get(0).getLabel());
            mRealm.commitTransaction();
        }

        RealmResults<DataStore> dataStore = mRealm.where(DataStore.class).findAll();
        uiAdapter = new UiAdapter(this, dataStore);
        mListView.setAdapter(uiAdapter);
        spinner.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    /**
     * Network Availablity Check
     * @return
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
