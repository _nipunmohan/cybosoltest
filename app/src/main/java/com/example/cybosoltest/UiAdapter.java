package com.example.cybosoltest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.cybosoltest.Data.DataStore;

import java.util.List;

/**
 * Created by Nipun on 3/23/2017.
 */

public class UiAdapter extends ArrayAdapter<DataStore> {

    public UiAdapter(Context context, List<DataStore> posts) {

        super(context, 0, posts);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataStore entry = getItem(position);


        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.entry_row, parent, false);


        TextView name = (TextView) convertView.findViewById(R.id.txtName);
        TextView summary = (TextView) convertView.findViewById(R.id.txtSummary);
        TextView price = (TextView) convertView.findViewById(R.id.txtPrice);
        ImageView image = (ImageView) convertView.findViewById(R.id.imgView);
        name.setText(entry.getName());
        summary.setText(entry.getSummary());
        price.setText(entry.getPrice() + " " + entry.getCurrency());
        Glide.with(getContext()).load(entry.getImageUrl()).into(image);
        return convertView;
    }
}
